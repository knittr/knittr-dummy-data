var express = require('express');
var cookieParser = require("cookie-parser");
var morgan = require("morgan");
var bodyParser = require("body-parser");
var path = require('path');
var data = require('./data');

var app = express();
app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname,"public")));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.post('/feed', function(req, res) {
    //res.json(data.articles);
    setTimeout(() => {
      res.json(data.feeds);
    }, 1000);
  });

app.route('/article')
  .post(function(req, res) {
    var id = req.body.id;
    var article = data.articles.filter(item => item.id == id);
    //res.json(article[0]);
    if(article.length > 0) {
      setTimeout(() => {
        res.json(article[0]);
      }, 1000);
    } else {
      res.json({ invalid: true });
    }
  });

app.post('/bookmarks', function(req, res) {
  var articles = data.articles.slice(13);
  res.json(articles);
     /* setTimeout(() => {
      res.json(articles);
     }, 2000); */
});

app.post('/search', function(req, res) {
  var name = req.body.title;
  if(name === "error") {
     res.json({ invalid: true});
  } else {
    var articles = data.articles.slice(13);
    res.json(articles);
    /*  setTimeout(() => {
      res.json(articles);
     }, 2000); */
  }
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(3000, function() {
  console.log("listening on port 3000....");
});